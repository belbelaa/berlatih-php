<?php

// Di dalam file tersebut buatlah function dengan nama tentukan_nilai yang menerima parameter berupa integer. dengan ketentuan jika paramater integer lebih besar dari sama dengan 85 dan lebih kecil sama dengan 100 maka akan mereturn String “Sangat Baik” Selain itu jika parameter integer lebih besar sama dengan 70 dan lebih kecil dari 85 maka akan mereturn string “Baik” selain itu jika parameter number lebih besar sama dengan 60 dan lebih kecil dari 70 maka akan mereturn string “Cukup” selain itu maka akan mereturn string “Kurang”

function tentukanNilai($angka) {
    if ($angka >= 85 && $angka <= 100) {
        echo "Sangat Baik <br>";
    } else if ($angka >= 70 && $angka < 85) {
        echo "Baik <br>";
    } else if ($angka >= 60 && $angka < 70) {
        echo "Cukup <br>";
    } else {
        echo "Kurang <br>";
    }
}

//TEST CASES
echo tentukanNilai(98); //Sangat Baik
echo tentukanNilai(76); //Baik
echo tentukanNilai(67); //Cukup
echo tentukanNilai(43); //Kurang

?>